//LIBRARIES
#include "Ultrasonic.h"
#include <dht.h>
#include <BH1750FVI.h>
#include <Wire.h>
//END LIBRARIES

#define HOST_NAME "testejarvisproject1.azurewebsites.net"
#define SSID "network"
#define PASSWORD "password12"
#define HOST_PORT   80

/* Region DEFINE Device Properties */
String DEVICEID = "IDDEVICE";

/* Region ActionsToPerform */
#define TURNLIGHTON  "turnonlight"
#define TURNLIGHTOFF  "turnofflight"
#define TURNFANON  "turnonfan"
#define TURNFANOFF  "turnofffan"
/* End Region ActionsToPerform */

/* Region DEFINE Inputs Actuators */
#define RELELIGHT  8
#define RELEFAN  9
/* End Region DEFINE Inputs Actuators */

/* Region DEFINE Iputs Sensors */ 
dht DHT;
#define TEMPSENSOR  2

#define SOUNDSENSOR  3
#define MAGNETICSENSOR  4
#define MOVIMENTSENSOR  5

#define DISTANCESENSOR  6
int triggerDistancePin = 10;
int echoDistancePin = 11;
Ultrasonic ultrasonic(triggerDistancePin, echoDistancePin);

#define GASSENSOR  A0
#define GASSENSORLIMIT 50

BH1750FVI LightSensor;
/* End Region DEFINE Inputs Sensors */

/* Region InputLayer Variables */
boolean lastReleLight = false;
boolean lastReleFan = false;
boolean lastMagnetic = false;
boolean lastFire = false;
boolean lastPresence = false;
double lastTemp = 0;
double lastHumidity = 0;
double lastDistance = 0; 
uint16_t lastLight = 0;

boolean currentReleLight = false;
boolean currentReleFan = false;
boolean currentMagnetic = false;
boolean currentFire = false;
boolean currentPresence = false;
double currentTemp = 0;
double currentHumidity = 0;
double currentDistance = 0; 
uint16_t currentLight = 0;
/* End Region InputLayer Variables */

/* Region DEFINE URLS */
#define URLSENDENVIRONMENTSTATUS  "POST /api/Device/PostUpdateEnvironment HTTP/1.1\r\n"
#define URLGETACTIONS  "GET /api/Device/GetActionsToPerform?deviceId=" + DEVICEID + " HTTP/1.1\r\n"
/* End Region DEFINE URLS */


void setup() {
  delay(5000);
  pinMode(RELELIGHT, OUTPUT); 
  pinMode(RELEFAN, OUTPUT);
  delay(1000);
  
  pinMode(TEMPSENSOR, INPUT);
  delay(1000);

  //MOVIMENT REGION
  pinMode(MOVIMENTSENSOR, INPUT);
  delay(1000);
  //END MOVIMENT REGION

  //LIGHT REGION
  LightSensor.begin();
  LightSensor.SetAddress(Device_Address_H);
  LightSensor.SetMode(Continuous_H_resolution_Mode);
  //END LIGHT REGION
  
  Serial.begin(115200);
  Serial.print("AT+RST\r\n");
  delay(2000);
  Serial.print("AT\r\n");
  delay(1000);

  String connectToNetwork = "AT+CWJAP=\"" + SSID + "\", \"" + PASSWORD + "\"\r\n";
  Serial.print(connectToNetwork);
  delay(5000);
}

void loop() {
  currentValues();
  bool getJarvisActions = updateEnvironmentStatus();
  
  String TCPConnection = "AT+CIPSTART=\"TCP\",\"";
  TCPConnection += HOST_NAME;
  TCPConnection += "\",";
  TCPConnection += HOST_PORT;
  TCPConnection += "\r\n";

  long int time = millis();

  Serial.print(TCPConnection);
  delay(2000);

  String dataToSend = "";
  String getString = "";
  if(getJarvisActions){
    dataToSend = "{DeviceId:\"" + DEVICEID + "\",ReleLight:" + currentReleLight + 
    ",ReleFan:" + currentReleFan + ",Temp:" + currentTemp + 
    ",Humidity:" + currentHumidity + ",Presence:" + currentPresence + 
    ",Magnetic:" + currentMagnetic + ",Fire:" + currentFire + 
    ",Distance:" + currentDistance + ",Light:" + currentLight +"}";    
    getString = URLSENDENVIRONMENTSTATUS;
  }else {
    getString = URLGETACTIONS;
  } 
  
  getString += "Host: testejarvisproject1.azurewebsites.net\r\n";
  getString += "Content-Type: application/json\r\n";
  getString += "Data-Type: application/json\r\n";
  getString += "Content-Length: ";
  getString += dataToSend.length();
  getString += "\r\n\r\n";
  getString += dataToSend;

  String cipSend = "AT+CIPSEND=";
  cipSend += String(getString.length());
  cipSend += "\r\n";
  Serial.print(cipSend);
  time = millis();
  while((time + 1000) > millis());
  Serial.print(getString);

  String returnCall = "";
  time = millis();
  while ((time + 20000) > millis())
  {    
    while(Serial.available()){
      char c = Serial.read();
      returnCall += c;
    }    
  }

  if(returnCall.indexOf(TURNFANOFF) >= 0){
  }else {
      if(returnCall.indexOf(TURNFANON) >= 0){
        changeFanStatus(true);
      }
  }

  if(returnCall.indexOf(TURNLIGHTOFF) >= 0){
      changeLightStatus(false);
    }else {
      if(returnCall.indexOf(TURNLIGHTON) >= 0){
        changeLightStatus(true);
      }
    }
    
  Serial.print("AT+CIPCLOSE\r\n");

  delay(2000);
}

void changeLightStatus(bool turnOn){
  if(turnOn){
    digitalWrite(RELELIGHT, LOW);
  }else {
    digitalWrite(RELELIGHT, HIGH);
  }
}

void changeFanStatus(bool turnOn){
  if(turnOn){
    digitalWrite(RELEFAN, LOW);
  }else {
    digitalWrite(RELEFAN, HIGH);
  }
}

bool updateEnvironmentStatus (){
  if(lastReleLight != currentReleLight ||
     lastReleFan != currentReleFan ||
     lastMagnetic != currentMagnetic ||
     lastFire != currentFire ||
     lastPresence != currentPresence ||
     continuousValuesChange(currentTemp, lastTemp) ||
     continuousValuesChange(currentHumidity,lastHumidity) ||
     lightChanged(currentLight,lastLight)){
    updateValues();
    return true;
  }
}

bool continuousValuesChange(double currentValue, double lastValue){
  if((currentValue > (lastValue + 1.0)) || 
     (currentValue < (lastValue - 1.0))){
    return true;
  }
  return false;
}

bool lightChanged(double currentValue, double lastValue){
  if((currentValue > (lastValue + 30.0)) || 
     (currentValue < (lastValue - 30.0))){
    return true;
  }

  return false;
}

void updateValues(){
  lastReleLight = currentReleLight;
  lastReleFan = currentReleFan;
  lastMagnetic = currentMagnetic;
  lastFire = currentFire;
  lastPresence = currentPresence;

  if(continuousValuesChange(currentTemp, lastTemp)){
    lastTemp = currentTemp;
  }

  if(continuousValuesChange(currentHumidity, lastHumidity)){
    lastHumidity = currentHumidity;
  }
  
  if(lightChanged(currentLight, lastLight)){
    lastLight = currentLight; 
  }
}

void currentValues(){
  currentReleLight = digitalRead(RELELIGHT);
  currentReleFan = digitalRead(RELEFAN);
  long int time  = millis();
  while((time + 1000) > millis());
  int smokeGasValue = analogRead(GASSENSOR); //Faz a leitura da entrada do sensor
  smokeGasValue = map(smokeGasValue, 0, 1023, 0, 100);
  if(smokeGasValue > GASSENSORLIMIT){
    currentFire = true;
  }else {
    currentFire = false;
  }
  time = millis();
  while((time + 1000) > millis());
  
  //TEMPERATURE REGION
  DHT.read11(TEMPSENSOR);
  currentHumidity = DHT.humidity;
  currentTemp = DHT.temperature;
  time = millis();
  while((time + 1000) > millis());
  //END TEMPERATURE REGION
  
  //REGION DISTANCE
  currentDistance = ultrasonic.Ranging(CM);
  time = millis();
  while((time + 1000) > millis());
  //END DISTANCE REGION

  boolean auxMoviment = digitalRead(MOVIMENTSENSOR);
  if(auxMoviment == true || currentDistance < 50){
    currentPresence = true;
  }else {
    currentPresence = false;
  }
  time = millis();
  while((time + 1000) > millis());

  uint16_t currentLight = LightSensor.GetLightIntensity();
}

